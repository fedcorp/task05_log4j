package com.fedcorp;
import org.apache.logging.log4j.*;

public class Log4jDemo {
    private final static  Logger logger = LogManager.getLogger(Log4jDemo.class.getName());
    public static void main(String[] args) {
        logger.trace("trace message");
        logger.debug("debug message");
        logger.info("info message");
        logger.warn("warn message");
        logger.error("error message");
        logger.fatal("fatal message");
    }
}
